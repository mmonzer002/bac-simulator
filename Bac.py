from pyModbusTCP.client import ModbusClient
from pyModbusTCP.server import ModbusServer
import math

SERVER_PORT = 1502


class Tank():
    def __init__(self, modbus_client=None):
        # the master who initiate requests (only master can initiate conversation)
        self.c = modbus_client

    # global
    # Ts : sampling time
    Ts = 0.01

    X1 = 0  # valve A is closed
    X2 = 0  # valve solvant is closed
    Xout = 0  # valve out is closed
    Qc = 0  # volumetric flow rate of the cooling (from article)

    # outputs
    T = 350
    T10 = 350   # temperature of A
    T20 = 350   # temperature of solvant
    Q1 = 100    # () measurement of input flow rate
    Q2 = 100

    # assume the minimum volume in the tank is 0.001
    VMin = .001
    V = 0       # the volume of the reactor
    V1 = 0      # the volume of A in the reactor
    V2 = 0      # the volume of B in the reactor
    Vout = 0    # the volume of out


    CA = 0      # concentration of A
    Qout = 100  # measurment of output flow rate

    TC0 = 350   # inlet coolant temperature

    # parameters
    k0 = 7.2E10  # reaction rate constant
    E_R = 1E4  # activation energy to R
    deltaH = -2E5  # heat reaction
    cp = 1  # specific heat of the reactant
    cpc = 1  # specific heat of the cooling
    rho = 1E3  # density of the reactant
    rhoc = 1E3  # density of the cooling
    cA0 = 1  # feed concentration
    hA = 7E5  # heat transfer coefficient

    agitation = 0
    agitation_time = 0

    # region -- read/write reigsters and coils
    # read / write coil values
    def write_coil_value(self, bit_addr, bit_value):
        self.c.write_single_coil(bit_addr, bit_value)

    def read_coil_value(self, reg_addr, reg_nb=1):
        return self.c.read_coils(reg_addr, reg_nb)[0]

    # read / write register values
    def write_register_value(self, reg_addr, reg_value):
        self.c.write_single_register(reg_addr, reg_value)

    def read_register_value(self, reg_addr, reg_nb=1):
        return self.c.read_input_registers(reg_addr, reg_nb)[0]

    # endregion -- read / write register values

    # region -- control valve
    def open_x1(self):
        self.write_coil_value(100, 1)  # X1 open
        self.X1 = 1

    def close_x1(self):
        self.write_coil_value(100, 0)  # X1 close
        self.X1 = 0

    def open_x2(self):
        self.write_coil_value(101, 1)  # X2 open
        self.X2 = 1

    def close_x2(self):
        self.write_coil_value(101, 0)  # X2 close
        self.X2 = 0

    def open_xout(self):
        self.write_coil_value(102, 1)  # Xout open
        self.Xout = 1

    def close_xout(self):
        self.write_coil_value(102, 0)  # Xout close
        self.Xout = 0

    def start_agitation(self):
        self.write_coil_value(301, 1)
        self.agitation = 1

    def stop_agitation(self):
        self.write_coil_value(301, 0)
        self.agitation = 0
    # endregion


    def initalize_coils_and_registers(self):
        self.close_x1();
        self.close_x2();
        self.close_xout();
        self.stop_agitation()

        self.write_register_value(103, 80)  # Qc

        self.write_register_value(200, 350)  # Temperature = Reactant's feed temperature
        self.write_register_value(300, 360)


    def computeControl(self):
        '''
            here we are simulating the controller's work (what should be done by the plc to keep temp acceptable)

                the plc should request Q up when temp up to cool the system
                the plc should request Q down when temp down to keep temp in a range
        '''
        Tset = self.read_register_value(300)  # readRegisterValue(300, 1000);

        T = self.read_register_value(200)  # readRegisterValue(200, 1000);

        self.Qc = self.read_register_value(103)  # readRegisterValue(103, 1000);
        self.Qc -= (Tset - T) * .5

        if self.Qc > 500:
            self.Qc = 500
        if self.Qc < 00:
            self.Qc = 0

        self.write_register_value(103, self.Qc)  # writeRegisterValue(103, Qc, 1000);


    def compute_v1(self):
        self.V1 += self.Q1 * self.X1 * self.Ts/10

    def compute_v2(self):
        self.V2 += self.Q2 * self.X2 * self.Ts/10

    def compute_vout(self):
        delta_vout = self.Qout * self.Xout * self.Ts/10
        if delta_vout == 0 or self.V <= self.VMin:
            return
        self.Vout += self.Qout * self.Xout * self.Ts / 10

        # if self.V > self.VMin:
        #     if delta_vout > 0 and delta_vout > self.V:
        #         self.Vout += self.Qout * self.Xout * self.Ts/10
        #     else:
        #         self.Vout += self.V

    def compute_alim_volumes(self):
        self.compute_v1()
        self.compute_v2()
        self.compute_vout()

    # region -- tank's environment
    def do_process_step(self):
        Q1 = self.Q1
        Q2 = self.Q2
        Qout = self.Qout

        F1 = Q1 * self.X1  # flow 1
        F2 = Q2 * self.X2  # flow 1
        Fout = Qout * self.Xout  # flow out

        # region -- calculate the new volume in the tank
        self.compute_alim_volumes()             # calculate the volume of each alim
        dV = F1 + F2 - Fout
        self.V = self.V + dV * self.Ts / 10     # total tank's volume

        self.V = 200 if self.V > 200 else self.V
        self.V = self.VMin if self.V < self.VMin else self.V
        # endregion -- calculate the new volume in the tank

        is_agitation_started = self.read_coil_value(301)
        if is_agitation_started == 0:
            return

        self.agitation_time += self.Ts / 10

        # region -- constants
        a1 = self.V1 / self.V  # equation (2)
        a5 = self.V2 / self.V  # extension to the case, equation is the same as a1
        a6 = self.Vout / self.V  # extension to the case, equation is the same as a1

        # calculate a2, a3 and a4 using equation (2)
        a2 = -self.deltaH / (self.rho * self.cp)
        a3 = (self.rhoc * self.cpc) / (self.rho * self.cp * self.V)
        a4 = -self.hA / (self.rhoc * self.cpc)

        # calculate k1 using equation (3)
        k1 = self.k0 * math.exp(-self.E_R / self.T)  # reaction rate

        # if Qc is stopped, it won't affect the temperature anymore. right?
        dT_coulant_part = a3 * self.Qc * (1 - math.exp(a4 / self.Qc)) * (self.TC0 - self.T) if self.Qc > 0 else 0

        # delta_T equation is extended to support 2 inputs and 1 output
        dT = a1 * self.T10 + a5 * self.T20 - a6 * self.T + \
             a2 * k1 * self.CA + dT_coulant_part
        dCA = a1 * self.cA0 - a6 * self.CA - k1 * self.CA


        # change current values (temp, concentation of A, volume in the tank)
        self.T += dT * self.Ts / 10  # the process is to be done 10 times
        self.CA = self.CA + dCA * self.Ts / 10


        # keep measures in an accepted ranges
        self.T = 0 if self.T < 0 else self.T

        self.CA = 0 if self.CA < 0 else self.CA

    # endregion -- tank's environment

    def simulProcess(self):
        self.X1 = self.read_coil_value(100)
        self.X2 = self.read_coil_value(101)
        self.Xout = self.read_coil_value(102)
        self.Qc = self.read_register_value(103)

        print "Qc=" + self.Qc.__str__()

        # wait for some time leaving the environment's measures change, before recalculating measures
        for index in range(100):
            self.do_process_step()

        self.write_register_value(200, self.T)      # will be changed during operation
        self.write_register_value(201, self.T10)    # the temp of 1 shouldn't change during operation
        self.write_register_value(202, self.T20)    # the temp of 2 shouldn't change during operation
        self.write_register_value(203, self.Q1)     # the maximum flow rate of 1 shouldn't change during operation
        self.write_register_value(204, self.Q2)     # the maximum flow rate of 2 shouldn't change during operation
        self.write_register_value(205, self.V)      # could be changed during operation
        self.write_register_value(206, self.CA)     # could be changed during operation
        self.write_register_value(207, self.Qout)   # the maximum flow rate of out shouldn't change during operation
        self.write_register_value(208, self.TC0)


    def open_x1_x2(self):
        self.open_x1()
        self.open_x2()
        self.close_xout()

    # def start_simulation(self):
    #     client = self.c
    #     if client.open():
    #         self.initalize_coils_and_registers()
    #
    #         for index in range(1000):
    #             self.computeControl()
    #             self.simulProcess()
    #             print "Time=" + (
    #                 index * self.Ts).__str__() + "\n" + self.V.__str__() + "   T=" + self.T.__str__() + "    CA=" + self.CA.__str__() + "  V=" + self.V.__str__()
    #     else:
    #         print "not open"

def main():
    print "simulating ... "
    unit_id = 1
    modbus_client = ModbusClient(host="localhost", port=SERVER_PORT, auto_open=True, unit_id=unit_id)

    tank = Tank(modbus_client)
    client = tank.c

    # client.debug(True)

    if client.open():
        tank.initalize_coils_and_registers()

        for index in range(1000):
            tank.computeControl()
            tank.simulProcess()
            print "Time=" + (index * tank.Ts).__str__() + "\n" + tank.V.__str__() + "   T=" + tank.T.__str__() + "    CA=" + tank.CA.__str__() + "  V=" + tank.V.__str__()
    else:
        print "not open"

def create_client():
    modbus_client = ModbusClient(host="localhost", port=SERVER_PORT, auto_open=True)
    modbus_client.open()
    tank = Tank(modbus_client)
    tank.read_register_value(300)

create_client()
# main()
