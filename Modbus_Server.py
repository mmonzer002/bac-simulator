from pyModbusTCP.server import ModbusServer
SERVER_PORT = 1502

server = ModbusServer(host='localhost', port=SERVER_PORT)

server.start()
