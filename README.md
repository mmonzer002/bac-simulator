Context
--------
Write a python pg which simulates the bins (for the reaction tank) all delta T, it reads the inputs (the valves) and calculates the evolution. The ctrl will aim to make a manufacturing recipe (it opens the valves, heats, waits, ...) and the top we put the IDS.

How to run this app 
----------------------------------------------------
- You can create a virtual environment to install dependencies of this app without having to worry about conflicts with
 other dependencies on your pc, to do so
    1. Create the virtual environment (**optional**)
        
        if you have a linux/osx, you can use virtualenv.
        once it's installed type the following command in your terminal:
            
            virtualenv venv
        
    2. Activate the virtual environment
     
            source venv/bin/activate
            
- Install project dependencies
    
        pip install -r requirements.txt
        
- Activate project

    1. activate the server (Modbus Slave)
    
            python Modbus_server.py
            
            
        
    2. activate the client (Modbus Master)
    
            python GUI.py
            
          [![Simulator.png](https://s2.postimg.org/6o4385max/Simulator.png)](https://postimg.org/image/rl0bctkbp/)

Future work
-------
- Equations used in the Bac.py needs to be reviewed
    
    What to be **fixed** in the next version:
    
        - Assuming that the Qc is the # volumetric flow rate of the cooling (from article)
            for time = t, Qc can be 0 (no coolant in activated): 
                - this will create a problem in Equation (1)
                (cannot divide by zero)
                reference: Bac.py line 204
                
        - The change of the temperature is related to the flow of product A (a1 = q/v)?
        It shouldn't be related to the volume of a mixing of products in the tank ?
        Note: (assuming that the reaction is exothermic)  (I need confirmation here) 
            I changed the equation to use the existing volume of A and B in the tank instead of the flow
            
        - I am not sure about the equation used to calculate the concentration of A
        
        
            
        
- Live sniff network packets
- parse modbus tcp packets (make modbus packet human friendly)
         
        
        
