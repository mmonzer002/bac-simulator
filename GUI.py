import threading

from pyModbusTCP.client import ModbusClient
from tkinter import *
import tkinter.messagebox
from threading import Thread
from time import sleep
from Bac import Tank


root=Tk()
root.title(string='Simulator')
width = 550
height = 300
root.geometry(width.__str__() + 'x' + height.__str__() +'+200+200')     # window's size
root.attributes("-topmost", True)


frame1=Frame(root)
frame1.pack(side=TOP,fill=X)

qrString = IntVar(None)
label11 = Label(frame1, text="volumetric flow rate of the reactant qr").grid(row=0)
entry1 = Entry(frame1, textvariable=qrString, state=DISABLED).grid(row=0, column=1)
label12 = Label(frame1, text="l.min-1").grid(row=0, column=2)

qcString = IntVar(value=0)
label2 = Label(frame1, text="volumetric flow rate of the coolant qc").grid(row=1)
entry2 = Entry(frame1, textvariable=qcString).grid(row=1, column=1)
label22 = Label(frame1, text="l.min-1").grid(row=1, column=2)

tr0String = IntVar(value=360)
label3 = Label(frame1, text="input temperature of the reactant Tr0").grid(row=2)
entry3 = Entry(frame1, textvariable=tr0String).grid(row=2, column=1)
label32 = Label(frame1, text="K").grid(row=2, column=2)

tc0String = IntVar(value=350)
label4 = Label(frame1, text="input temperature of the coolant Tc0").grid(row=3)
entry4 = Entry(frame1, textvariable=tc0String).grid(row=3, column=1)
label42 = Label(frame1, text="K").grid(row=3, column=2)

c0String = IntVar(None)
label5 = Label(frame1, text="input concentration of the reactant c0").grid(row=4)
entry5 = Entry(frame1, textvariable=c0String, state=DISABLED).grid(row=4, column=1)
label52 = Label(frame1, text="mol.l-1").grid(row=4, column=2)

# **** StatusBar ******************
statusText2 = StringVar(value='checking machine status...')
status2= Label(root, textvariable=statusText2, bd=1,relief=SUNKEN,anchor=W)
status2.pack(side=BOTTOM, fill=X)

statusText = StringVar(value='idle')
status= Label(root, textvariable=statusText,bd=1,relief=SUNKEN,anchor=W)
status.pack(side=BOTTOM, fill=X)
# *********************************


thread = None
stop_event = threading.Event()


# region -- on button click
SERVER_PORT = 1502
unit_id = 1
tank = None

def on_start_simulation():
    global tank

    statusText.set('simulation started')
    modbus_client = ModbusClient(host="localhost", port=SERVER_PORT, auto_open=True, unit_id=unit_id)
    modbus_client.debug(True)
    tank = Tank(modbus_client)
    client = tank.c

    Qr = qrString.get()
    tank.Qc = qcString.get()
    tank.T = tr0String.get()
    tank.TC0 = tc0String.get()
    C0 = c0String.get()

    thread = Thread(target= start_simulation, args= (tank,))
    thread.start()

    enable_control_buttons()
    button_start_simulation['state'] = DISABLED
    button_stop_simulation['state'] = NORMAL


def on_stop_simulation():
    stop_event.set()
    statusText.set('end.')
    disable_control_buttons()
    button_start_simulation['state'] = NORMAL
    button_stop_simulation['state'] = DISABLED
    print "---------------- end -------------------"


def on_open_x1():
    if(tank):
        if(tank.X1):
            tank.close_x1()
            button_open_x1_text.set('open x1')
        else:
            tank.open_x1()
            button_open_x1_text.set('close x1')

def on_open_x2():
    if(tank):
        if (tank.X2):
            tank.close_x2()
            button_open_x2_text.set('open x2')
        else:
            tank.open_x2()
            button_open_x2_text.set('close x2')

def on_open_xout():
    if(tank):
        if (tank.Xout):
            tank.close_xout()
            button_open_xout_text.set('open xout')
        else:
            tank.open_xout()
            button_open_xout_text.set('close xout')

def on_start_agitation():
    if (tank):
        tank.start_agitation()

def on_stop_agitation():
    if (tank):
        tank.stop_agitation()

def disable_control_buttons():
    button_open_x1['state'] = DISABLED
    button_open_x2['state'] = DISABLED
    button_open_xout['state'] = DISABLED
    button_start_agitation['state'] = DISABLED
    button_stop_agitation['state'] = DISABLED

def enable_control_buttons():
    button_open_x1['state'] = NORMAL
    button_open_x2['state'] = NORMAL
    button_open_xout['state'] = NORMAL
    button_start_agitation['state'] = NORMAL
    button_stop_agitation['state'] = NORMAL

def start_simulation(tank):
    client = tank.c
    if client.open():
        tank.initalize_coils_and_registers()

        # for index in range(1000):
        index = 0
        while not stop_event.wait(1):
            x1_status = 'open' if tank.X1 else 'closed'
            x2_status = 'open' if tank.X2 else 'closed'
            xout_status = 'open' if tank.Xout else 'closed'
            agitation_status = 'started' if tank.agitation else 'stopped'

            tank.computeControl()
            tank.simulProcess()
            console=  "Time=" + (index * tank.Ts).__str__() + "   T=" + tank.T.__str__() + "    Qc=" + tank.Qc.__str__() + \
                      "  V=" + tank.V.__str__() + "(" + "A=" + tank.V1.__str__() + " , B=" + tank.V2.__str__() + " , out=" + tank.Vout.__str__() +")"
            console2= "X1=" + x1_status + "  X2=" + x2_status + "  Xout=" + xout_status + "   agitation=" + agitation_status + "    agitation_time=" + tank.agitation_time.__str__()
            print console
            statusText.set(console)
            statusText2.set(console2)
            index += 1

        stop_event.clear()
    else:
        print "not open"

# endregion -- on button click

button_start_simulation = Button(frame1, text='start simulation', command=on_start_simulation)
button_start_simulation.grid(row=5, sticky=W)

button_stop_simulation = Button(frame1, text='stop simulation', command=on_stop_simulation, state=DISABLED)
button_stop_simulation.grid(row=5, column=1, sticky=W)

button_open_x1_text = StringVar(value='open x1')
button_open_x1 = Button(frame1, textvariable=button_open_x1_text, command=on_open_x1, state=DISABLED)
button_open_x1.grid(row=6, column=0, sticky=W)

button_open_x2_text = StringVar(value='open x2')
button_open_x2 = Button(frame1, textvariable=button_open_x2_text, command=on_open_x2, state=DISABLED)
button_open_x2.grid(row=6, column=1, sticky=W)

button_open_xout_text = StringVar(value='open xout')
button_open_xout = Button(frame1, textvariable=button_open_xout_text, command=on_open_xout, state=DISABLED)
button_open_xout.grid(row=6, column=2, sticky=W)

button_start_agitation_text = StringVar(value='start agitation')
button_start_agitation = Button(frame1, textvariable=button_start_agitation_text, command=on_start_agitation, state=DISABLED)
button_start_agitation.grid(row=7, column=0, sticky=W)

button_stop_agitation_text = StringVar(value='stop agitation')
button_stop_agitation = Button(frame1, textvariable=button_stop_agitation_text, command=on_stop_agitation, state=DISABLED)
button_stop_agitation.grid(row=7, column=1, sticky=W)


root.mainloop()



# region -- create the window
# app = Tk()                          # window
# app.title("GUI Example")            # window's title
# width = 500
# height = 600
# app.geometry(width.__str__() + 'x' + height.__str__() +'+200+200')     # window's size
# # endregion -- create the window
#
# frame1 = Frame(app).pack(side=TOP, fill=X)
#
# qrString = IntVar(None)
# label11 = Label(frame1, text="volumetric flow rate of the reactant qr").grid(row=0)
# entry1 = Entry(frame1, textvariable=qrString, state=DISABLED).grid(row=0, column=1)
# label12 = Label(frame1, text="l.min-1").grid(row=0, column=2)
#
# qcString = IntVar(value=0)
# label2 = Label(frame1, text="volumetric flow rate of the coolant qc").grid(row=1)
# entry2 = Entry(frame1, textvariable=qcString).grid(row=1, column=1)
# label22 = Label(frame1, text="l.min-1").grid(row=1, column=2)
#
# tr0String = IntVar(None)
# label3 = Label(frame1, text="input temperature of the reactant Tr0").grid(row=2)
# entry3 = Entry(frame1, textvariable=tr0String, state=DISABLED).grid(row=2, column=1)
# label32 = Label(frame1, text="K").grid(row=2, column=2)
#
# tc0String = IntVar(value=350)
# label4 = Label(frame1, text="input temperature of the coolant Tc0").grid(row=3)
# entry4 = Entry(frame1, textvariable=tc0String).grid(row=3, column=1)
# label42 = Label(frame1, text="K").grid(row=3, column=2)
#
# c0String = IntVar(None)
# label5 = Label(frame1, text="input concentration of the reactant c0").grid(row=4)
# entry5 = Entry(frame1, textvariable=c0String, state=DISABLED).grid(row=4, column=1)
# label52 = Label(frame1, text="mol.l-1").grid(row=4, column=2)
#
# thread = None
# stop_event = threading.Event()
#
#
# # region -- on button click
# SERVER_PORT = 1502
# unit_id = 1
# def on_start_simulation():
#
#     modbus_client = ModbusClient(host="localhost", port=SERVER_PORT, auto_open=True, unit_id=unit_id)
#     tank = Tank(modbus_client)
#     client = tank.c
#
#     Qr = qrString.get()
#     tank.Qc = qcString.get()
#     Tr0 = tr0String.get()
#     tank.TC0 = tc0String.get()
#     C0 = c0String.get()
#
#     thread = Thread(target= start_simulation, args= (tank,))
#     thread.start()
#
#
# def on_stop_simulation():
#     stop_event.set()
#     print "---------------- end -------------------"
#
#
# def start_simulation(tank):
#     client = tank.c
#     if client.open():
#         tank.initalize_coils_and_registers()
#
#         # for index in range(1000):
#         index = 0
#         while not stop_event.wait(1):
#             tank.computeControl()
#             tank.simulProcess()
#             print "Time=" + (
#                 index * tank.Ts).__str__() + "\n" + tank.V.__str__() + "   T=" + tank.T.__str__() + "    CA=" + tank.CA.__str__() + "  V=" + tank.V.__str__()
#             index += 1
#     else:
#         print "not open"
#
# # endregion -- on button click
#
# button = Button(frame1, text='start simulation', command=on_start_simulation).grid(row=5, sticky=W)
# button_stop_simulation = Button(frame1, text='stop simulation', command=on_stop_simulation).grid(row=5, column=1, sticky=W)
#
# app.mainloop()